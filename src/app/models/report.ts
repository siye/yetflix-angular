export class Report{
	constructor(
		public _id: string, 
        public user: any , 
        public film: any , 
        public date: string, 
        public action: string,
        public state: string
	){}
}