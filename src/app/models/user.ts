export class User{
	constructor(
		public id: string, 
		public name: string,
		public surname: string,
		public nick: string,
		public email: string,
		public password: string,
		public bornDate: any,
		public phone: string,
		public role: string
	){}
}