export class Activity{
	constructor(
		public _id: string, 
        public user: any , 
        public date: string, 
        public action: string,
        public complement: string
	){}
}