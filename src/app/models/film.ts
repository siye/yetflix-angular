export class Film{
	constructor(
		public _id: string, 
		public title: string,
		public synopsis: string,
		public genre: string,
		public long: string,
		public year: string,
		public colection: string,
		public director: string,
		public actor: string,
        public trailer: string,
        public img: string,
		public uri: string,
		public uri2: string,
		public uri3: string,
		public uri4: string,
		public uri5: string
		//public seen: number
	){}
}

