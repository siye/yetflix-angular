import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
//cargamos routing
import { routing, appRoutingProviders } from './app.routing';
//para subir archivos
import { AngularFileUploaderModule } from "angular-file-uploader";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { LoginComponent } from './components/login/login.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { ErrorComponent } from './components/error/error.component';
import { FooterComponent } from './components/footer/footer.component';
import { DescpageComponent } from './components/descpage/descpage.component';
import { NewFilmComponent } from './components/new-film/new-film.component';
import { UpdateFilmComponent } from './components/update-film/update-film.component';
import { DeleteFilmComponent } from './components/delete-film/delete-film.component';
import { GenreListComponent } from './components/genre-list/genre-list.component';
import { SearchComponent } from './components/search/search.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ActivitiesComponent } from './components/activities/activities.component';
import { ReportComponent } from './components/report/report.component';//Angular no pierda url al recargar aplicacion

//cargamos en ngModule la configuracion del routing (en imports y providers)
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    WelcomeComponent,
    LoginComponent,
    SignInComponent,
    ErrorComponent,
    FooterComponent,
    DescpageComponent,
    NewFilmComponent,
    UpdateFilmComponent,
    DeleteFilmComponent,
    GenreListComponent,
    SearchComponent,
    ActivitiesComponent,
    ReportComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    HttpClientModule,
    AngularFileUploaderModule
  ],
  providers: [
    appRoutingProviders,
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
