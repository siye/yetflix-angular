//componentes necesarios del core de angular
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//importamos nuestros componentes creados
import { LoginComponent } from './components/login/login.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { HomeComponent } from './components/home/home.component';
import { ErrorComponent } from './components/error/error.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { DescpageComponent } from './components/descpage/descpage.component';
import { NewFilmComponent } from './components/new-film/new-film.component';
import { UpdateFilmComponent } from './components/update-film/update-film.component';
import { DeleteFilmComponent } from './components/delete-film/delete-film.component';
import { GenreListComponent } from './components/genre-list/genre-list.component';
import { SearchComponent } from './components/search/search.component';
import { ActivitiesComponent } from './components/activities/activities.component';
import { ReportComponent } from './components/report/report.component';


//enlazamos rutas con sus componentes
const appRoutes: Routes = [
    {path: '', component: HomeComponent},
    {path: 'bienvenido', component: WelcomeComponent},
    {path: 'login', component: LoginComponent},
    //{path: 'logout', component: LoginComponent},
    {path: 'logout/:goOut', component: LoginComponent},
    {path: 'registro', component: SignInComponent},
    {path: 'error', component: ErrorComponent},
    {path: 'desc/:id', component: DescpageComponent},
    {path: 'new', component: NewFilmComponent},
    {path: 'update/:id', component: UpdateFilmComponent},
    {path: 'delete/:id', component: DeleteFilmComponent},
    {path: 'genre/:genre', component: GenreListComponent},
    {path: 'genre/:genre/:page', component: GenreListComponent},
    {path: 'search/:searchStr', component: SearchComponent},
    {path: 'activities', component: ActivitiesComponent},
    {path: 'reports', component: ReportComponent},
    {path: '**', component: ErrorComponent}//poner siempre el último 
    
];

//exportamos la configuracion
export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);