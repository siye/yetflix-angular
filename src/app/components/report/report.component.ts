import { Component, OnInit, DoCheck } from '@angular/core';
import { ReportService } from '../services/report.service';
import { Report } from '../../models/report';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import {Router, ActivatedRoute, Params } from '@angular/router';


@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  providers: [ ReportService, UserService, FilmService, ActivityService]
})
export class ReportComponent implements OnInit {
  public page_title;
  public reports: Report[];
  public othersReports: Report[];
  public status;
  
  constructor(
    private _reportService : ReportService,
    private _activityService : ActivityService,
    private _router : Router,
    private _route : ActivatedRoute,
    
  ) {

    
   }
   
  ngOnInit() {
    this.page_title =  'Avisos de errores en Yetflix ';
    this.getReports();
  }

  ngDoCheck() {
    this.getReports();
  }


  //listar todos los reports
  getReports(){
      this._reportService.getReports().subscribe(
      response => {
        this.reports = response.reports;
        this.othersReports = response.othersReports;
      },
      error => {
        console.log(error);
      }
    );
  }

  //actualizamos report
  updateComplete(id, state, title){
      //peticion a servicio de report para modificar estado a C o B (completado, borrado)
      this._reportService.update(id, state).subscribe(
        response=>{
          if(response.status == 'success'){
            this.status = 'success';
            //console.log("Modificado y actividad creada" + id + '  ' + state+ '  ' + name);
          
            //registro de actividad
            this._activityService.addActivity('modificacion de alerta',title);

            //redireccion
            //this._router.navigate(["/reports"]);
          
          }else{
            this.status = "error";
          }
        },
        error=>{
          this.status = "error";
          console.log(error);
          console.log("</br> error report en update" );
          
        }
    )
  }

  

}//end class
