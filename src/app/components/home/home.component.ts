import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { global } from '../services/global';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [UserService, FilmService, DatePipe, ActivityService]
})
export class HomeComponent implements OnInit {
  public identity;
  public token;
  //public films;
  public news;
  public newsTerror;
  public newsFiccion;
  public newsAnimation;
  public url;
  public loading: boolean;
  public today;
  public expirationDateLogin;
  
  constructor(
    private _userService: UserService,
    private _filmService: FilmService,
    private _activityService : ActivityService,
    private _router : Router
    
  ) { 
    //console.log("url constructor home");
    this.url = global.url;
    //console.log(this.url);
  }

  ngOnInit() {
    this.getFilms();
    this.loadUSer();
    this.loading = false;
  }

  loadUSer(){
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.expirationDateLogin = this._userService.getExpirationDateLogin();
    
    //check expirition date login 1 week
    this.today = new Date();
    //console.log(this.expirationDateLogin);
    if(this.expirationDateLogin && this.today > this.expirationDateLogin){
      //console.log(this.today > this.expirationDateLogin);
      //if(confirm(" Hola " + this.identity.nick +" ¿Sigues siendo tu? Logueate para demostrarlo! ")) {
          localStorage.removeItem('identity');
          localStorage.removeItem('token');
          localStorage.removeItem('expirationDateLogin');
       // }
    }else if(this.expirationDateLogin === null){//borramos sesion sin fecha (si existe) obligando asi al login
      localStorage.removeItem('identity');
      localStorage.removeItem('token');
      localStorage.removeItem('expirationDateLogin');
    }
  }
  //listar todas las pelis
  getFilms(){
      this._filmService.getFilms().subscribe(
      response => {
        //this.films = response.films;
        this.news = response.news;
        this.loading = true;
      },
      error => {
        console.log(error);
      }
    );
  }

  //registramos actividad
  goToaddActivity(accion, complemento){
    this._activityService.addActivity(accion, complemento);
  }



}
