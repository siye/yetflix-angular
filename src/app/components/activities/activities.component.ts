import { Component, OnInit } from '@angular/core';
import { ActivityService } from '../services/activity.service';
import { Activity } from '../../models/activity';
import { UserService } from '../services/user.service';
import {Router, ActivatedRoute, Params } from '@angular/router';
//import { global } from '../services/global';
//import { identity } from 'rxjs';

declare var $: any; //for jQuery
@Component({
  selector: 'app-activities',
  templateUrl: './activities.component.html',
  styleUrls: ['./activities.component.css'],
  providers: [ ActivityService, UserService]
})
export class ActivitiesComponent implements OnInit {
  public page_title;
  public activities: Activity[];
  //public identity;
  public vio;
  public realizo;
   
  constructor(
    private _activityService : ActivityService,
    private _router : Router,
    private _route : ActivatedRoute,
    private _userService: UserService,
  ) { 
    this.vio = 'vió';
    this.realizo = 'realizó';
  }

  ngOnInit() {
    this.page_title =  'Actividad en YETFLIX ';
    this.getActivities();
    
  }
  

  //listar todas las actividades
  getActivities(){
    this._activityService.getActivities().subscribe(
    response => {
      this.activities = response.activities;
      
      

      //console.log(this.activities);
    },
    error => {
      console.log(error);
    }
  );
  }


}//end class
