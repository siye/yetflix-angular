import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { User } from '../../models/user';
import { UserService } from '../services/user.service';
import { ActivityService } from '../services/activity.service';
import { NgForm } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService,DatePipe,ActivityService]
})
export class LoginComponent implements OnInit {
    public page_title: string;
    public user: User;
    public identity : string;
    public token : string;
    public status: string;
    public today;
    public suma7dias;

    constructor(
      private _userService : UserService,
      private _activityService : ActivityService,
      private _router : Router,
      private _route : ActivatedRoute
    ) {
      this.page_title = "Login" 
      this.user = new User ('1', '', '', '', '', '', '', '', 'user' );
    }

    ngOnInit() {
      //console.log("entro en init login");
      //console.log(this.user);
      this.logout();
    }

    onSubmit(form: NgForm){
      //console.log('Your form data : ', form.value);
      this._userService.login(this.user).subscribe(
        response=>{
            if(! response.status  || response.status !="error"){
                this.status = 'success';
                this.identity = JSON.stringify(response.user);
                this.token = response.token;
                //console.log (this.identity);
                //console.log( this.token);

                //expiracion del login ( 1 semana )
                this.today = new Date();
                // (días * 24 horas * 60 minutos * 60 segundos * 1000 milésimas de segundo) 
                this.suma7dias = this.today.getTime()+(7*24*60*60*1000); 
                
                //metemos datos de logeo en almacenamiento local
                localStorage.setItem('token', this.token);
                localStorage.setItem('identity', this.identity);
                localStorage.setItem('expirationDateLogin', this.suma7dias);

                //registramos actividad
                this._activityService.addActivity('login','');

                //redireccion
                this._router.navigate(["/"]);
            }else{
                
                this.status = "error";
            }
        },
        error => {
          this.status = "error";
          //console.log(this.user);
        }
      )
    }

    logout(){
      //console.log("params");
      //console.log("entro en logout");
        this._route.params.subscribe(params =>{
          let goOut = +params['goOut'];
          if (goOut == 1 ){

              //registramos actividad
              this._activityService.addActivity('logout','');

              localStorage.removeItem('identity');
              localStorage.removeItem('token');
              localStorage.removeItem('ExpirationDateLogin');

              this.identity = null;
              this.token = null;
              this.suma7dias = null;

              //redireccion
              this._router.navigate(["/"]);
          }
        });
      
    }
    

}
