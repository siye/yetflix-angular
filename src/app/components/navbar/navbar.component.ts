import { Component, OnInit, DoCheck} from '@angular/core';
import {Router } from '@angular/router';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { ReportService } from '../services/report.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
  providers: [UserService, FilmService, ActivityService,ReportService]
})
export class NavbarComponent implements OnInit, DoCheck {

  public identity;
  public token;
  public admin;
  public vip;
  public genres;
  public genreClicked;
  public search;
  public status;
  public alert;
  public res;
  
  
  constructor(
    private _userService: UserService,
    private _filmService: FilmService,
    private _activityService : ActivityService,
    private _router : Router,
    private _reportService : ReportService
    
  ) { 
    
    this._filmService.getGenres().subscribe(
      response => {
        this.genres = response.genres;
        //console.log(this.genres);
      },
      error =>{
        console.log(error);
      }
    );
    


  }

  ngOnInit() {
    //comprobamos alertas para mostrar campana de aviso
    this.alert = false;
    this._reportService.getReports().subscribe(
      response => {
          this.res = response.reports;
          //console.log(this.alert.length);
          if(this.res.length != 0 ) {
            this.alert = true;
            //console.log("en true ");
          }
          //console.log(this.alert);  
      },
      error => {
        console.log(error);
      }
    ); 
  }

  ngDoCheck() {
    this.loadUSer();
  }

  loadUSer(){
    this.identity = this._userService.getIdentity();
    if(this.identity){
      this.vip= this._userService.getVip(this.identity.role);
      this.admin= this._userService.getAdmin(this.identity.role);
    }
    this.token = this._userService.getToken();
  }
  //redireccionmos a pag de generos pasandole el genero clickeado
  goToGenre(genre){
    //alert(genre);
    //redireccion
    this._router.navigate(["/genre", genre]);
  }
  //capturamos string del campo busqueda, y lo incorporamos a la url xa ser recogido x el componente search
  goToSearch(){
    //alert(this.search);
    //registramos actividad
    this._activityService.addActivity('busqueda', this.search);
    //redireccion
    this._router.navigate(["/search", this.search]);
  }

  //registramos actividad
  goToaddActivity(accion, complemento){
    this._activityService.addActivity(accion, complemento);
  }
  
}
