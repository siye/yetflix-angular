import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { Film } from '../../models/film';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { NgForm } from '@angular/forms';
import { global } from '../services/global';


@Component({
  selector: 'app-new-film',
  templateUrl: './new-film.component.html',
  styleUrls: ['./new-film.component.css'], 
  providers:[UserService, FilmService, ActivityService]
})
export class NewFilmComponent implements OnInit {

  public page_title: string;
  public film : Film;
  public status: string;
  public identity;
  public token;
  public url;
  
  

  constructor(
    private _userService : UserService,
    private _filmService : FilmService,
    private _activityService : ActivityService,
    private _router : Router,
    private _route : ActivatedRoute
    
  ) { 
    this.page_title = 'Añadir nueva película ';
    this.film = new Film('1','','','','','','','','','','','','','','','');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url = global.url;
  }

  ngOnInit() {
  
  }

  onSubmit(form: NgForm){
    //console.log('Your form data : ', form.value);

    // ******procesamos links externos
      //trailer Youtube
      var isNewTra = this.film.trailer.indexOf("youtube");
      if(isNewTra != -1){
        var trailerForm = this.film.trailer.split("?v=");
        var trailerLink = trailerForm[1];
        this.film.trailer = trailerLink;
        //console.log(trailerLink);
      }else{
        this.film.trailer = '';
      }

      
      //img de Imgur
      var imgForm = this.film.img.split("/");
      var imgLink = imgForm[3].split("[");
      //console.log(imgLink[0]);
      this.film.img = imgLink[0];
    
      //video de Fembed
      if(this.film.uri){
        var uriForm= this.film.uri.split("/");
        var uriLink = uriForm[4];
        this.film.uri = uriLink;
      //console.log(uriForm);
      }

      //video de MyStream
      if(this.film.uri2){
        var uri2Form= this.film.uri2.split("https://mystream.to/watch/");
        this.film.uri2 = uri2Form[1];
      }
     // console.log(this.film.uri2);

      //video de UqLoad
      if(this.film.uri3){
        var uri3Form= this.film.uri3.split("https://uqload.com/");
        this.film.uri3 = uri3Form[1];
      }
      //console.log(this.film.uri3);

      //video de vidia
      if(this.film.uri4){
        var uri4Form= this.film.uri4.split("https://vidia.tv/");
        this.film.uri4 = uri4Form[1];
      }

      //video de JetLoad
      if(this.film.uri5){
        var uri5Form= this.film.uri5.split("https://jetload.net/p/");
        this.film.uri5 = uri5Form[1];
      }

      // ****************** NI CASO DEL LowerCase ----- titulo en minus xa bd
      var title = this.film.title.toLowerCase();
      this.film.title = title;

      this.film.colection = this.film.colection.replace(/ /g, "");
      
      console.log('Your form data : ', this.film);

    //peticion a servicio de pelis->metodo register para registar peli mediante peticion a la api
    
    this._filmService.create(this.token, this.film).subscribe(
        response=>{
          if(response.status == 'success'){
            this.status = 'success';
            //registro de actividad
            this._activityService.addActivity('añadir peli',this.film.title);
            //redireccion
            this._router.navigate(["/"]);
          }else{
            this.status = "error";
          }
        },
        error=>{
          this.status = "error";
          console.log(error);
        }

    ) 
    
  }

}
