import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { Film } from '../../models/film';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { global } from '../services/global';

@Component({
  selector: 'app-delete-film',
  templateUrl: './delete-film.component.html',
  styleUrls: ['./delete-film.component.css'],
  providers:[UserService, FilmService]
})
export class DeleteFilmComponent implements OnInit {

  public film : Film;
  public status: string;
  public identity;
  public token;

  constructor(
      private _userService : UserService,
      private _filmService : FilmService,
      private _router : Router,
      private _route : ActivatedRoute
  ) { 
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    this.deleteFilm();
  }

  deleteFilm(){
    this._route.params.subscribe(params =>{
      let id = params['id'];
     
      this._filmService.delete(this.token, id).subscribe(
        response=>{
          if(response.status == 'success'){
            this.film = response.film;
            console.log("entro en borrado");
            //redireccion
            this._router.navigate(["/"]);
          }
        },
        error=>{
          console.log(error);
          this.status = "error";
        }
      )
    })
  }

}
