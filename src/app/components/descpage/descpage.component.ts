import { Component, OnInit } from '@angular/core';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { ReportService } from '../services/report.service';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { global } from '../services/global';

import { DomSanitizer, SafeUrl, SafeResourceUrl} from '@angular/platform-browser';
import { identity } from 'rxjs';
declare var $: any; //for jQuery
@Component({
  selector: 'app-descpage',
  templateUrl: './descpage.component.html',
  styleUrls: ['./descpage.component.css'],
  providers: [UserService, FilmService, ActivityService, ReportService]
})
export class DescpageComponent implements OnInit {

  public identity;
  public film;
  public token;
  public status;
  public vip;
  public admin;
  public itemUrl : string;
  public genuineUrl : SafeResourceUrl;
  public genuineUrl2 : SafeResourceUrl;
  public genuineUrl3 : SafeResourceUrl;
  public genuineUrl4 : SafeResourceUrl;
  public genuineUrl5 : SafeResourceUrl;
  public genuineUrl6 : SafeResourceUrl;
  public genuineUrl7 : SafeResourceUrl;
  public colectionList;
  public idUsuario;

  constructor(
    private _userService: UserService,
    private _filmService : FilmService,
    private _activityService : ActivityService,
    private _reportService : ReportService,
    private _router : Router,
    private _route : ActivatedRoute,
    private sanitizer: DomSanitizer
    
  ) { 
      this.token = this._userService.getToken();
      this.identity = this._userService.getIdentity();
      if (this.identity){
        this.vip = this._userService.getVip(this.identity.role);
        this.admin = this._userService.getAdmin(this.identity.role);
        this.idUsuario = this.identity._id;
      }
    }

  ngOnInit() {
     this.getFilm();
  }

  getFilm(){
    this._route.params.subscribe(params =>{
      let id = params['id'];

      this._filmService.getFilm(id).subscribe(
        response=>{
          if(response.status == 'success'){
            this.film = response.film;
            this._filmService.showTrueValues(this.film);
            this.safeImg(this.film.img);
            this.safeTrailer(this.film.trailer);
            if(this.film.uri!=" "){ 
              this.safeVideo(this.film.uri); 
            }else{
              this.film.uri=undefined;
            }
            this.safeVideo(this.film.uri);
            if(this.film.uri2!=" "){  
              this.safeVideo2(this.film.uri2);
            }else{
              this.film.uri2=undefined;
            }
            if(this.film.uri3!=" "){  
              this.safeVideo3(this.film.uri3);
            }else{
              this.film.uri3=undefined;//si esta vacion en la bd le damos valor indefinido xa k no salga la pestaña de esta opcion
            }
            if(this.film.uri4!=" "){  
              this.safeVideo4(this.film.uri4);
            }else{
              this.film.uri4=undefined;
            }
            if(this.film.uri5!=" "){  
              this.safeVideo5(this.film.uri5);
            }else{
              this.film.uri5=undefined;
            }
            this.colectionList = this.film.colection.split(",");
            
          
            // registramos actividad
            this._activityService.addActivity('pelicula', this.film.title);
          }
        },
        error=>{
          this.status = "error";
          console.log(error);
        }
      )
    })
  }

  safeImg(url){
    this.itemUrl = 'https://i.imgur.com/'+url;
    this.genuineUrl = this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
    //console.log (this.genuineUrl);
  }

  safeTrailer(url){
    this.itemUrl = 'https://www.youtube.com/embed/'+url;
    this.genuineUrl2 = this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
    //console.log (this.genuineUrl2);
  }

  safeVideo(url){
    this.itemUrl = 'https://feurl.com/v/'+url;
    this.genuineUrl3= this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
    //console.log (this.genuineUrl);
  }
  safeVideo2(url){
      this.itemUrl = 'https://embed.mystream.to/'+url;
      this.genuineUrl4= this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
    //console.log (this.genuineUrl);
  }
  safeVideo3(url){
      this.itemUrl = 'https://uqload.com/embed-'+url;
      this.genuineUrl5= this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
    //console.log (this.genuineUrl);
  }
  safeVideo4(url){
    this.itemUrl = 'https://vidia.tv/embed-'+url;
    this.genuineUrl6= this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
  //console.log (this.genuineUrl);
  }
  safeVideo5(url){
    this.itemUrl = 'https://jetload.net/e/'+url;
    this.genuineUrl7= this.sanitizer.bypassSecurityTrustResourceUrl(this.itemUrl);
  //console.log (this.genuineUrl);
  }

  clickMethod(name: string, id: string) {
    if(confirm("¿¿SEGURO?? Vas a borrar la peli  "+ name + "!! \n (╯°□°） Esto quedará bajo tu conciencia...")) {
      //registramos actividad
      this._activityService.addActivity('eliminar peli', name);
      //redireccion
      this._router.navigate(["/delete", id]);
    }
  }

  
  clickWarning(name: string, id: string, title: string) {
    if(confirm("Gracias por poner el aviso  "+ name + " \n  En breve estará arreglado, disfruta de otra peli mientras.")) {
      $("#btnWarning").prop( "disabled", true );
      //registramos report
      //console.log(this.idUsuario);
      this._reportService.addReport(this.idUsuario, id, 'alert');
      
      //registramos actividad
      this._activityService.addActivity('añadir reporte', title);    
    }
  }


}
