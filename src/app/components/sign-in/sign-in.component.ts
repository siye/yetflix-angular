import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { UserService } from '../services/user.service';
import { ActivityService } from '../services/activity.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
  providers: [UserService, ActivityService]
})
export class SignInComponent implements OnInit {

  public page_title: String;
  public user: User;
  public status: String;
  public updating: boolean;

  constructor(
    private _userService : UserService,
    private _activityService : ActivityService,
  ) {
    this.page_title = "Registro" 
    this.user = new User ('1', '', '', '', '', '', '', '', 'user' );
    this.updating = false;
  }

  ngOnInit() {
    //console.log(this.user);
    //console.log(this._userService.prueba());
    alert("OJITO esto no es gratis! Pide 5 pavos al nuevo pavo! ");

  }

  onSubmit(form: NgForm){
    //console.log('Your form data : ', form.value);
    //console.log(this.user);

    //peticion a servicio de usuarios->metodo register para registar usuario mediante peticion a la api
    this._userService.register(this.user).subscribe(
        response=>{
          if(response.status == 'success'){
            this.status = 'success';
            //registramos actividad
            this._activityService.addActivity('registro usuario', this.user.nick);
            form.reset();
          }else{
            this.status = "error";
          }
        },
        error=>{
          this.status = "error";
          //console.log(error);
        }

    )
  }

}
