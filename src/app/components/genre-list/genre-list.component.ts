import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { global } from '../services/global';
import { NavbarComponent } from '../navbar/navbar.component';
import { ActivitiesComponent } from '../activities/activities.component';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css'],
  providers: [UserService, FilmService, ActivityService]
})
export class GenreListComponent implements OnInit {

  public identity;
  public token;
  public films;
  public url;
  public loading: boolean;
  public number_pages;
  public prev_page;
  public next_page;
  public totalPages;
  public genre;
  public page_title;

  public noPaginate: boolean;
  public vacio: boolean;
  
  constructor(
    private _userService: UserService,
    private _filmService: FilmService,
    private _activityService : ActivityService,
    private _router : Router,
    private _route : ActivatedRoute
  ) {
    this.url = global.url;
    
   }

  ngOnInit() {
    //console.log(this._route.params);
    this._route.params.subscribe(params =>{
      this.genre =  params['genre'];
      if(this.genre){
        this.page_title = this.genre;
        this._route.params.subscribe(params =>{
          var page =  params['page'];
          if(page){
            this.getFilmsByGenre(this.genre , page);
          }else{
            this.getFilmsByGenre(this.genre , 1);
          }
        });  
      }
    });  
    this.loading = false;
    
  }
 
//listar todas las pelis del genero
  getFilmsByGenre(genre , page){
        page = parseInt(page);
        this._filmService.getFilmsByGenre( genre, page).subscribe(
        response => {
          //Navegacion de la paginacion
          this.totalPages = response.totalPages;
          var number_pages2 = [];
          for(var i= 1; i <= this.totalPages; i++){
            number_pages2.push(i);
          }
          this.number_pages = number_pages2;
          if( page >=2){
              this.prev_page = page-1;
          }else{
            this.prev_page = 1
          }
          if( page < this.totalPages){
            this.next_page = page+1;
          }else{
            this.next_page = this.totalPages;
          }
          //console.log("Total"+this.totalPages);
          //console.log("previa"+this.prev_page+ "  actual" + page + "  siguiente" +this.next_page);
      
          this.films = response.films;
          this.loading = true;
        },
        error => {
          console.log(error);
        }
      );
  }// end getFilmGenre

  goToGenrePaginate(page){
    //redireccion
    this.page_title = this.genre;
    this._router.navigate(["/genre", this.genre , page]);
  }

  
  
}
