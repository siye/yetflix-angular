import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { Film } from '../../models/film';
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';
import { ActivityService } from '../services/activity.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-film',
  templateUrl: '../new-film/new-film.component.html',
  styleUrls: ['../new-film/new-film.component.css'],
  providers:[UserService, FilmService,ActivityService]
})
export class UpdateFilmComponent implements OnInit {

  public page_title: string;
  public film : Film;
  public status: string;
  public identity;
  public token;
  
  constructor(
    private _userService : UserService,
    private _filmService : FilmService,
    private _activityService : ActivityService,
    private _router : Router,
    private _route : ActivatedRoute
  ) { 
    this.page_title = 'Editar película ';
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    
  }

  ngOnInit() {
    this.getFilm();
  }

  getFilm(){
    this._route.params.subscribe(params =>{
      let id = params['id'];
     
      this._filmService.getFilm(id).subscribe(
        response=>{
          if(response.status == 'success'){
            this.film = response.film;
            //console.log("peli obtenida en getFilm update ")
            //console.log(this.film);
            this._filmService.showTrueValues(this.film);
          }
        },
        error=>{
          console.log(error);
          this.status = "error";
        }
      )
    })
  }

  onSubmit(form: NgForm){
    //console.log('Your form data : ', form.value);

    // ******procesamos links externos - ** Implementar API de cada uno en un futuro

    //vemos si ha introducido un video nuevo, si no esta la palabra clave de cada link (youtube, fembed e imgur) en el campo, es k no se ha modificado
      //trailer Youtube
      var isNewTra = this.film.trailer.indexOf("youtube");
      if(isNewTra != -1){
        var trailerForm = this.film.trailer.split("?v=");
        var trailerLink = trailerForm[1];
        this.film.trailer = trailerLink;
      }
      //img de Imgur
      var isNewImg = this.film.img.indexOf("imgur");
      if(isNewImg != -1){
        var imgForm = this.film.img.split("/");
        var imgLink = imgForm[3].split("[");
        this.film.img = imgLink[0];
      }
      //video de Fembed
      var isNewVid = this.film.uri.indexOf("fembed");
      if(isNewVid != -1){
        var uriForm= this.film.uri.split("/");
        var uriLink = uriForm[4];
        this.film.uri = uriLink;
      }

      //video de MyStream
      var isNewVid2 = this.film.uri2.indexOf("mystream");
      if(isNewVid2 != -1){
        var uri2Form= this.film.uri2.split("https://mystream.to/watch/");
        this.film.uri2 = uri2Form[1];
      }

      //video de UqLoad
      var isNewVid3 = this.film.uri3.indexOf("uqload");
      if(isNewVid3 != -1){
        var uri3Form= this.film.uri3.split("https://uqload.com/");
        this.film.uri3 = uri3Form[1];
      }
       //video de vidia
      var isNewVid4 = this.film.uri4.indexOf("vidia");
      if(isNewVid4 != -1){
        var uri4Form= this.film.uri4.split("https://vidia.tv/");
        this.film.uri4 = uri4Form[1];
      }
       
      //video de JetLoad
      var isNewVid5 = this.film.uri5.indexOf("jetload");
      if(isNewVid5 != -1){
        var uri5Form= this.film.uri5.split("https://jetload.net/p/");
        this.film.uri5 = uri5Form[1];
      }
     
      this.film.colection = this.film.colection.replace(/ /g, "");
      
      //alert(this.film.colection);
    //peticion a servicio de pelis->metodo register para registar peli mediante peticion a la api
    this._filmService.update(this.token, this.film, this.film._id).subscribe(
        response=>{
          if(response.status == 'success'){
            this.status = 'success';
            //console.log("peli mandada en el form");
            //console.log(this.film);
             //registro de actividad
             this._activityService.addActivity('editar peli',this.film.title);
            //redireccion
            this._router.navigate(["/"]);
          }else{
            this.status = "error";
          }
        },
        error=>{
          this.status = "error";
          console.log(error);
          console.log("</br> peli error form en update" );
          console.log(this.film);
        }
    )
  }
}
