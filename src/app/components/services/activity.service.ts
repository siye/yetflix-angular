
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { global } from './global'; 
import { UserService } from '../services/user.service';

@Injectable()
export class ActivityService {

    public url: string;
    public identity;
    public status;
    public user;

    constructor(
        public _http: HttpClient,
        private _userService: UserService
    ){
        this.url = global.url;
    }
    
  //crea actividad
  addActivity(actividad, complemento){
    //comprobar  complemento
    complemento = (complemento) ? complemento : " "; 
    //comprobar identidad del usuario, sino DESCONOCIDO
    this.identity = this._userService.getIdentity();
    this.user =  (this.identity) ? this.identity._id : "5e9d78f9c974570004d8872a"; 
    
    //crear actividad nueva
    this.create(this.user, actividad, complemento).subscribe(
      response=>{
        if(response.status == 'success'){
          this.status = 'success';
        }else{
          this.status = "error";
        }
      },
      error=>{
        this.status = "error";
        console.log(error);
      }
    )
}
  //peticion a api para registrar actividad del usuario
  create(id, accion, complemento):Observable<any>{
      let params =  JSON.stringify( { "user": id, "action": accion, "complement" : complemento} );
      //console.log(params);
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      return this._http.post(this.url+'addActivity', params, {headers:headers});
  }

  //listar todas las actividades
  getActivities():Observable<any>{
  let headers = new HttpHeaders().set('Content-Type', 'application/json');
  return this._http.get(this.url+'activities/',  {headers:headers});
}

    



   


    
}



































