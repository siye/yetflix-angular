import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { global } from './global'; 
import { UserService } from '../services/user.service';
import { FilmService } from '../services/film.service';

@Injectable()
export class ReportService {

    public url: string;
    public identity;
    public status;
    public film;
    public user;
    

    constructor(
        public _http: HttpClient,
        private _userService: UserService,
        private _filmService: FilmService
    ){
        this.url = global.url;
    }
    
  //crea actividad
  addReport(usuario, pelicula, accion){
    //crear actividad nueva
    this.create(usuario, pelicula, accion).subscribe(
      response=>{
        if(response.status == 'success'){
          this.status = 'success';
        }else{
          this.status = "error";
        }
      },
      error=>{
        this.status = "error";
        console.log(error);
      }
    )
}
  //peticion a api para registrar report del usuario
  create(usuario, pelicula, accion):Observable<any>{
      let params =  JSON.stringify( { "user": usuario, "film":pelicula, "action": accion} );
      //console.log(params);
      let headers = new HttpHeaders().set('Content-Type', 'application/json');
      return this._http.post(this.url+'addReport', params, {headers:headers});
  }

   //peticion xa actualizar report x su id 
  update(id, state ):Observable<any>{
      let params =  JSON.stringify({"state": state});
      let headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this._http.put(this.url+'updateReport/'+id, params, {headers:headers});
  }
  

  //listar todas los reports
  getReports():Observable<any>{
  let headers = new HttpHeaders().set('Content-Type', 'application/json');
  return this._http.get(this.url+'reports/',  {headers:headers});
}

    



   


    
}