import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../../models/user';
import { global } from './global'; 

@Injectable()
export class UserService {

    public url: string;
    public identity: string;
    public token: string;
    public vip: boolean;
    public admin: boolean;
    public expirationDateLogin;

    constructor(
        public _http: HttpClient
    ){
        this.url = global.url;
    }

    //peticion a api para registrar usuario
    register(user):Observable<any>{
         
        let params =  JSON.stringify(user);

        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+'registro', params, {headers:headers});
    }
    //peticion api para login
    login(user, gettoken = null):Observable<any>{

        if(gettoken != null){
            user.gettoken = "true";
        }
         
        let params =  JSON.stringify(user);

        let headers = new HttpHeaders().set('Content-Type', 'application/json');

        return this._http.post(this.url+'login', params, {headers:headers});
    }

    //metodo obtener identity del localStorage
    getIdentity(){
        let identity = JSON.parse(localStorage.getItem('identity'));

        if(identity && identity != 'undefined'){
            this.identity = identity;
        }else{
            this.identity = null;
        }
        return this.identity;
    }

    //metodo obtener token del localStorage
    getToken(){
        let token = localStorage.getItem('token');

        if(token && token != 'undefined'){
            this.token = token;
        }else{
            this.token = null;
        }
        return this.token;
    }

    //metodo obtener fecha expiracion del login  del localStorage
    getExpirationDateLogin(){
        let expirationDateLogin = localStorage.getItem('expirationDateLogin');

        if(expirationDateLogin && expirationDateLogin != 'undefined'){
            this.expirationDateLogin = expirationDateLogin;
        }else{
            this.expirationDateLogin = null;
        }
        return this.expirationDateLogin;
    }

    //metodo para saber si el usuario es vip (admin o cooperative) o no
    getVip(roleUser){
        this.vip = false;
        if(roleUser){
            if (roleUser == 'admin' || roleUser == 'cooperative'){
              this.vip = true;
            }
        }
        return this.vip;
    }

    //metodo para saber si el usuario es admin
    getAdmin(roleUser){
        this.admin = false;
        if(roleUser){
            if (roleUser == 'admin'){
              this.admin = true;
            }
        }
        return this.admin;
    }

     

    
}