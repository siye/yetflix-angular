import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Film } from '../../models/film';
import { global } from './global'; 

@Injectable()
export class FilmService {

    public url: string;
    constructor(
        public _http: HttpClient
    ){
        this.url = global.url;
    }

    //peticion a api para registrar pelicula
    create(token, film):Observable<any>{
        let params =  JSON.stringify(film);
        //console.log(params);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization', token);
        return this._http.post(this.url+'new', params, {headers:headers});
    }

    //listar todas las pelis
    getFilms():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'films/',  {headers:headers});
    }
    //peticion xa obtener peli x su id requiere login
    getFilm( id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'film/'+id, {headers:headers});
    }
     //peticion xa actualizar peli x su id requiere login
    update(token, film, id):Observable<any>{
        let params =  JSON.stringify(film);
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization', token);
        return this._http.put(this.url+'update/'+id, params, {headers:headers});
    }
    //peticion xa borrar peli x su id requiere login
    delete(token, id):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json')
                                        .set('Authorization', token);                           
        return this._http.delete(this.url+'delete/'+id, {headers:headers});
    }
    //en BD los campos vacios se guardan con espacio, estos los "vaciamos" para que no aparezca un espacio en la vista ni en los inputs
    showTrueValues(film: Film){ 
        if( film.genre      == ' ') { film.genre  = '' ; }
        if( film.long       == ' ') { film.long    = '' ; } 
        if( film.year       == ' ') { film.year    = '' ; } 
        if( film.colection  == ' ') { film.colection    = '' ; } 
        if( film.director   == ' ') { film.director    = '' ; } 
        if( film.actor      == ' ') { film.actor    = '' ; } 
        if( film.trailer    == ' ') { film.trailer    = '' ; }   

        return film;
    }
    //peticion xa obtener los generos usados, los cargamos en barra de navegacion
    getGenres():Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'genres/', {headers:headers});
    }
    //peticion obtener pelis segun genero clickeado 
    getFilmsByGenre(genre, page = 1):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'genre/'+genre+'/'+page, {headers:headers});
    }
    //peticion xa obtener peliculas coincidentes con el campo busqueda
    getSearch(searchStr):Observable<any>{
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this._http.get(this.url+'search/'+searchStr, {headers:headers});
    }

   

    



   


    
}