import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute, Params } from '@angular/router';
import { FilmService } from '../services/film.service';
@Component({
  selector: 'app-search',
  templateUrl: '../genre-list/genre-list.component.html',
  styleUrls: ['./search.component.css'],
  providers: [FilmService]
})
export class SearchComponent implements OnInit {
  public films;
  public loading: boolean;
  public noPaginate: boolean;
  public vacio: boolean;
  public page_title;

  public number_pages;
  public prev_page;
  public next_page;
  public totalPages;
  public genre;
  

  constructor(
    private _filmService: FilmService,
    private _route : ActivatedRoute,
  ) {
    this.loading = false;
    this.vacio = false;
    this.noPaginate = true;
   }

  ngOnInit() {
    this._route.params.subscribe(params =>{
      this.vacio = false;
      let searchStr = params['searchStr'];
      this.page_title =  'Con ' + searchStr + ' hemos encontrado: ';
      this.getFilmsSearched(searchStr);
    })
  }

  getFilmsSearched(searchStr){
      this._filmService.getSearch(searchStr).subscribe(
        response => {
          this.loading = true;
          this.films = response.films;
          console.log(this.films);
          if(this.films.length === 0) {
            this.vacio = true;
            //console.log("vacioo");
          }
         
        },
        error => {
          console.log(error);
        }
      );
  }

  goToGenrePaginate(page){
    //solo es xa k no salte el compilador, se usa el del componente genre
  }

}
